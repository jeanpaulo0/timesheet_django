# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Funcionario(models.Model):
    nome = models.CharField(max_length=255)
    data_admissao = models.DateField()
    SEXO_CHOICE = (
        ('m','Masculino'),
        ('f','Feminino'),
    )
    sexo = models.CharField(max_length=1, choices=SEXO_CHOICE)
    email = models.EmailField()
    