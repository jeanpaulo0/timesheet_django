# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import CreateView

from .forms import FuncionarioForm
from .models import Funcionario

def lista_funcionarios(request):
    funcionarios = Funcionario.objects.all()
    return render(request, 'funcionarios/lista.html', 
        {'funcionarios':funcionarios })

# class FuncionarioCreateView(CreateView):
#     form_class = FuncionarioForm
#     template_name = 'funcionarios/novo.html'
#     success_url = reverse_lazy('funcionarios:lista_funcionarios')
    
def novo_funcionario(request):

    if request.method == 'POST':
        form = FuncionarioForm(request.POST)
        if form.is_valid():
            obj = form.save()
            return HttpResponseRedirect(reverse('funcionarios:lista_funcionarios'))
    else:
        form = FuncionarioForm()

    return render(request, 
        'funcionarios/novo.html', {'form':form})

def editar_funcionario(request, pk):
    funcionario = Funcionario.objects.get(pk=pk)
    if request.method == 'POST':
        form = FuncionarioForm(request.POST, instance=funcionario)
        if form.is_valid():
            obj = form.save()
            return HttpResponseRedirect(reverse('funcionarios:lista_funcionarios'))
    else:
        form = FuncionarioForm(instance=funcionario)

    return render(request, 
        'funcionarios/editar.html', {'form':form})

