#coding: utf-8
from django.conf.urls import url

from .views import novo_funcionario, lista_funcionarios, editar_funcionario

urlpatterns = [
    url(r'^$', lista_funcionarios, name='lista_funcionarios'),
    url(r'^novo/$', novo_funcionario, name='novo_funcionario'),
    url(r'^(?P<pk>\d+)/editar/$', editar_funcionario, name='editar_funcionario'),
]