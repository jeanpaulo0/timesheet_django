#coding: utf-8
from django.conf.urls import url

from .views import novo_tomador, lista_tomadores, editar_tomador

urlpatterns = [
    url(r'^$', lista_tomadores, name='lista_tomadores'),
    url(r'^novo/$', novo_tomador, name='novo_tomador'),
    url(r'^(?P<pk>\d+)/editar/$', editar_tomador, name='editar_tomador'),
]