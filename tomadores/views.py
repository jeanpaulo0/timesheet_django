# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect

from .forms import TomadorForm
from .models import Tomador


def novo_tomador(request):
    if request.method == 'POST':
        form = TomadorForm(request.POST)
        if form.is_valid():
            obj = form.save()
            return HttpResponseRedirect(reverse('tomadores:lista_tomadores'))
    else:
        form = TomadorForm()

    return render(request
    	, 'tomadores/novo.html'
    	, {'form':form} 
    )

def lista_tomadores(request):
    tomadores = Tomador.objects.all()
    return render(request, 'tomadores/lista.html', 
        {'tomadores':tomadores })

def editar_tomador(request, pk):
    tomador = Tomador.objects.get(pk=pk)
    if request.method == 'POST':
        form = TomadorForm(request.POST, instance=tomador)
        if form.is_valid():
            obj = form.save()
            return HttpResponseRedirect(reverse('tomadores:lista_tomadores'))
    else:
        form = TomadorForm(instance=tomador)

    return render(request, 
        'tomadores/editar.html', {'form':form})
