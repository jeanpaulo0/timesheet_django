from django import forms
from .models import Tomador

class TomadorForm(forms.ModelForm):

    class Meta:
        model = Tomador
        fields = ['nome']